#include "sysdep.h"
#include "opcode/marie.h"

const marie_opc_info_t marie_opc_info[1 << 4] = {
	#define OP(TAG,OPCODE,MNEMONIC,argmask) \
	{TAG, MNEMONIC},
	#include "opcode/marie-insns.h"
};
