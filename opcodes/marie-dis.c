#include "sysdep.h"

#include "opcode/marie.h"
#include "disassemble.h"

extern const marie_opc_info_t marie_opc_info[1 << 4]; // Four bits per instruction (see bfd)

static fprintf_ftype fpr;
static void *stream;

#define instructionBits(b) ((b)[0] >> 4)
#define argumentBits(b) ((((b)[0] & 0XF) << 8) | (b)[1])



int
print_insn_marie(bfd_vma addr, struct disassemble_info *info)
{
	int status;
	stream = info->stream;
	unsigned char buffer[2];
	fpr = info->fprintf_func;

	if ((status = info->read_memory_func (addr, buffer, sizeof buffer, info)))
	{
		goto fail;
	}

	// 8 is maximum name length
	fpr (stream, "%-*s %03X", 8, marie_opc_info[instructionBits(buffer)].name , argumentBits(buffer));

	return sizeof buffer;

fail:
	info->memory_error_func(status, addr, info);
	return -1;
}
