#!/usr/bin/env bash
cat <<EOF
MEMORY
{
	ram (WX) : ORIGIN = 0x000, LENGTH = 4K
}

ORG = DEFINED(ORG) ? ORG : 0;
ENTRY(ORG)
SECTIONS
{
	.text ORG : {
		*(.text) 
	} > ram
}
EOF
