#ifndef MARIE_TDEP_H
#define MARIE_TDEP_H

struct gdbarch_tdep
{
	/* unused */
};

enum marie_regnum
{
  MARIE_AC_REGNUM = 0,
  MARIE_IR_REGNUM = 1,
  MARIE_MAR_REGNUM = 2,
  MARIE_MBR_REGNUM = 3,
  MARIE_PC_REGNUM = 4,
  MARIE_IN_REGNUM = 5,
  MARIE_OUT_REGNUM = 5,
};

#define MARIE_NUM_REGS 7

#endif /* moxie-tdep.h */
