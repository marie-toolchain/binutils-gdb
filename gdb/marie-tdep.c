#include "defs.h"
#include "gdbtypes.h"
#include "bfd.h"
#include "arch-utils.h"
#include "marie-tdep.h"
#include "osabi.h"
#include "frame-unwind.h"

/* Register names.  */

static const char *marie_register_names[MARIE_NUM_REGS] = {"AC",  "IR",  "MAR",  "MBR",  "PC", "InReg",  "OutReg"};

/* Implement the "register_name" gdbarch method.  */

#define lengthof(l) (sizeof(l) / sizeof((l)[0]))
static const char *
marie_register_name (struct gdbarch *gdbarch, int reg_nr)
{
  if (reg_nr < 0)
    return NULL;
  if (reg_nr >= lengthof(marie_register_names))
    return NULL;
  return marie_register_names[reg_nr];
}

static struct type *
marie_register_type (struct gdbarch *gdbarch, int reg_nr)
{
	switch (reg_nr) {
		case MARIE_PC_REGNUM:
			return builtin_type(gdbarch)->builtin_func_ptr;
		case MARIE_MAR_REGNUM:
			return builtin_type(gdbarch)->builtin_data_ptr;
		default:
			return builtin_type(gdbarch)->builtin_int16; // really int16 :/
	}
}

static
CORE_ADDR
marie_skip_prologue(
                    struct gdbarch *gdbarch,
                    CORE_ADDR pc
                    )
{
	/* prologue isnt a thing */
	return pc;
}

/* not sure what this is doing */

                                       /* bp_inst*/
static CORE_ADDR
marie_unwind_pc (struct gdbarch *gdbarch, struct frame_info *next_frame)
{
  return frame_unwind_register_unsigned (next_frame, MARIE_PC_REGNUM);
}

/* Given a GDB frame, determine the address of the calling function's
   frame.  This will be used to create a new GDB frame struct.  */

static void
marie_frame_this_id (struct frame_info *this_frame,
		    void **this_prologue_cache, struct frame_id *this_id)
{
	return;
}

constexpr gdb_byte marie_break_insn[] = { 0xF0, 0x00 }; // just an illegal instruction, for SIGILL

typedef BP_MANIPULATION (marie_break_insn) marie_breakpoint;

/* Get the value of register regnum in the previous stack frame.  */

static struct value *
marie_frame_prev_register (struct frame_info *this_frame,
			  void **this_prologue_cache, int regnum)
{
	printf("debug from %s, regnum = %i\n", __func__, regnum);
	return 0;
}

static const struct frame_unwind marie_frame_unwind = {
  NORMAL_FRAME,
  default_frame_unwind_stop_reason,
  marie_frame_this_id,
  marie_frame_prev_register,  
  NULL,
  default_frame_sniffer
};

static std::vector<CORE_ADDR>
marie_software_single_step (struct regcache *regcache) {
	printf("single stepping!\n");
  CORE_ADDR pc = regcache_read_pc (regcache);

	#define marie_next_pc(regcache, pc) ((pc) + 1) // TODO: fix
  CORE_ADDR next_pc = marie_next_pc (regcache, pc);
  return {next_pc};
}

static 
struct gdbarch *
marie_gdbarch_init(
                   struct gdbarch_info info,
                   struct gdbarch_list *arches
                   )
{
	struct gdbarch *gdbarch;
	struct gdbarch_tdep *tdep;
	arches = gdbarch_list_lookup_by_info (arches, &info);
	if (arches != NULL)
		return arches->gdbarch;

	tdep = XCNEW (struct gdbarch_tdep);
	gdbarch = gdbarch_alloc(&info, tdep);

	/* set variabes to struct */

	set_gdbarch_num_regs      (gdbarch, MARIE_NUM_REGS);
  set_gdbarch_pc_regnum     (gdbarch, MARIE_PC_REGNUM);
	set_gdbarch_register_name (gdbarch, marie_register_name);
  set_gdbarch_register_type (gdbarch, marie_register_type);
	set_gdbarch_skip_prologue (gdbarch, marie_skip_prologue);
	set_gdbarch_inner_than    (gdbarch, core_addr_lessthan);

	/* byte sizes! thank you tic for example code */
  set_gdbarch_ptr_bit (gdbarch, 32); // eh, would prefer 14 bits
  set_gdbarch_addr_bit (gdbarch, 14);
  set_gdbarch_short_bit (gdbarch, 16);
  set_gdbarch_int_bit (gdbarch, 16);
  set_gdbarch_long_bit (gdbarch, 16);
  set_gdbarch_long_long_bit (gdbarch, 16);
  //set_gdbarch_float_bit (gdbarch, 16);
  //set_gdbarch_double_bit (gdbarch, 16);

	/* what */
  set_gdbarch_breakpoint_kind_from_pc (gdbarch, marie_breakpoint::kind_from_pc);
	set_gdbarch_sw_breakpoint_from_kind (gdbarch, marie_breakpoint::bp_from_kind);
	/* single step breakpoint */
  //set_gdbarch_software_single_step (gdbarch, marie_software_single_step);
	(void) marie_software_single_step;

	/* unwinding */
  set_gdbarch_unwind_pc (gdbarch, marie_unwind_pc);
  frame_unwind_append_unwinder (gdbarch, &marie_frame_unwind);

	gdbarch_init_osabi (info, gdbarch);


	return gdbarch;
}

void
_initialize_marie_tdep (void)
{
  register_gdbarch_init (bfd_arch_marie, marie_gdbarch_init);
}
