/* marie-specific support for 32-bit ELF.
   Copyright (C) 2009-2019 Free Software Foundation, Inc.

   Copied from elf32-fr30.c which is..
   Copyright (C) 1998-2019 Free Software Foundation, Inc.

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#include "sysdep.h"
#include "bfd.h"
#include "libbfd.h"
#include "elf-bfd.h"
#include "elf/marie.h"

/* Forward declarations.  */

static reloc_howto_type marie_elf_howto_table [] =
{
	/* This reloc does nothing.  */
	HOWTO (R_MARIE_NONE,            /* type */
	       0,                       /* rightshift */
	       3,                       /* size (0 = byte, 1 = short, 2 = long) */
	       0,                       /* bitsize */
	       FALSE,                   /* pc_relative */
	       0,                       /* bitpos */
	       complain_overflow_dont,  /* complain_on_overflow */
	       bfd_elf_generic_reloc,   /* special_function */
	       "R_MARIE_NONE",          /* name */
	       FALSE,                   /* partial_inplace */
	       0,                       /* src_mask */
	       0,                       /* dst_mask */
	       FALSE),                  /* pcrel_offset */

	/* A 32 bit absolute relocation.  */
	HOWTO (R_MARIE_14,                 /* type */
	       0,                          /* rightshift */
	       1,                          /* size (0 = byte, 1 = short, 2 = long) */
	       14,                         /* bitsize */
	       FALSE,                      /* pc_relative */
	       0,                          /* bitpos */
	       complain_overflow_bitfield, /* complain_on_overflow */
	       bfd_elf_generic_reloc,      /* special_function */
	       "R_MARIE_14",               /* name */
	       FALSE,                      /* partial_inplace */
	       0x000,                      /* src_mask */
	       0xfff,                      /* dst_mask */
	       FALSE),                     /* pcrel_offset */
};

/* Map BFD reloc types to MARIE ELF reloc types.  */

struct marie_reloc_map
{
	bfd_reloc_code_real_type bfd_reloc_val;
	unsigned int marie_reloc_val;
};

static const struct marie_reloc_map marie_reloc_map [] =
{
	{ BFD_RELOC_NONE, R_MARIE_NONE },
	{ BFD_RELOC_14,   R_MARIE_14 },
};

static bfd_boolean
marie_info_to_howto (bfd *abfd ATTRIBUTE_UNUSED,
                     arelent *cache_ptr,
                     Elf_Internal_Rela *dst)
{
	unsigned int r_type = ELF32_R_TYPE (dst->r_info);
	BFD_ASSERT (r_type < (unsigned int) R_MARIE_max);

	cache_ptr->howto = &marie_elf_howto_table[r_type];
	return TRUE;
}

#define lengthof(array) (sizeof(array) / sizeof((array)[0]))
static reloc_howto_type *
marie_reloc_type_lookup (bfd *abfd ATTRIBUTE_UNUSED,
                         bfd_reloc_code_real_type code)
{
	for (unsigned int i = 0; i < lengthof(marie_reloc_map); i++)
	{
		if (marie_reloc_map[i].bfd_reloc_val == code)
			return &marie_elf_howto_table[marie_reloc_map[i].marie_reloc_val];
	}
	return NULL;
}

static reloc_howto_type *
marie_reloc_name_lookup(bfd *abfd ATTRIBUTE_UNUSED,
                        const char *r_name)
{
	for (unsigned int i = 0; i < lengthof(marie_elf_howto_table); i++)
	{
		char *name = marie_elf_howto_table[i].name;
		if(name != NULL && strcasecmp(name, r_name) == 0)
			return &marie_elf_howto_table[i];
	}
	return NULL;
}

#define TARGET_BIG_SYM  marie_elf32_vec
#define TARGET_BIG_NAME "elf32-marie"

#define ELF_ARCH         bfd_arch_marie
#define ELF_MACHINE_CODE EM_MARIE
#define ELF_MAXPAGESIZE  0x1

#define elf_info_to_howto               marie_info_to_howto

#define bfd_elf32_bfd_reloc_type_lookup marie_reloc_type_lookup
#define bfd_elf32_bfd_reloc_name_lookup marie_reloc_name_lookup

#include "elf32-target.h"
