#pragma once

enum __attribute__ ((packed)) marie_insn {
	#define OP(TAG,OPCODE,MNEMONIC,argmask) \
		TAG = (OPCODE),
	#include "marie-insns.h"
	#undef OP
};

typedef struct marie_opc_info_t
{
	enum marie_insn opcode;
	const char *name;
} marie_opc_info_t;
