/* Moxie Simulator definition.
   Copyright (C) 2009-2019 Free Software Foundation, Inc.
   Contributed by Anthony Green <green@moxielogic.com>

This file is part of GDB, the GNU debugger.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SIM_MAIN_H
#define SIM_MAIN_H

#include "sim-basics.h"
#include "sim-base.h"
#include "bfd.h"

#include "opcode/marie.h"

#define MBRIDX 3
#define PCIDX 4

#define NUM_MARIE_REGS 7

struct _sim_cpu {

  /* The following are internal simulator state variables: */

/* To keep this default simulator simple, and fast, we use a direct
   vector of registers. The internal simulator engine then uses
   manifests to access the correct slot. */

	#define CHAR_SIZE 8
	#define INST_SIZE 4
	#define WORD_SIZE 16 // why no WITH_TARGET_WORD_BITSIZE ?
	#define ADDR_SIZE 12 // why no WITH_TARGET_ADDRESS_BITSIZE ?

	union {
		unsigned_word regs[0]; // this just exists to index registers
		struct { /* is this a good idea? */
			unsigned_word AC:     WORD_SIZE;
			unsigned_word   :             0;
			union {
				unsigned_word byte: WORD_SIZE;
				struct {
					unsigned_word arg:   ADDR_SIZE; // ordered for little-endian (?)
					enum marie_insn opc: INST_SIZE; // this is definitely a hack
				} inst;
			} IR;
			unsigned_word   :             0;
			unsigned_word MAR:    ADDR_SIZE;
			unsigned_word   :             0;
			unsigned_word MBR:    WORD_SIZE;
			unsigned_word   :             0;
			unsigned_word PC:     ADDR_SIZE;
			unsigned_word   :             0;
			unsigned_word InReg:  CHAR_SIZE;
			unsigned_word   :             0;
			unsigned_word OutReg: CHAR_SIZE;
		};
	} registers;

  sim_cpu_base base;
};

struct sim_state {

  sim_cpu *cpu[MAX_NR_PROCESSORS];

  sim_state_base base;
};

#endif
