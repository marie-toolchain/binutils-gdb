#include "gdb/remote-sim.h"

#include "sim-main.h"
#include "sim-base.h"
#include "sim-options.h"
#include <stdbool.h>

#include "opcode/marie.h"

// 8 is maximum name length
#define MARIE_TRACE_INSN(str) \
	TRACE_INSN (cpu, "%-*s,\tAC: 0x%04x,\tIR: 0x%04x,\tIR.arg: 0x%03x,\tMAR: 0x%04x,\tMBR: 0x%04x,\tPC: 0x%04x,\tInReg: 0x%04x,\tOutReg: 0x%04x", \
	      8, str, cpu->registers.regs[0], cpu->registers.IR.byte, cpu->registers.IR.inst.arg, \
	      cpu->registers.regs[1], cpu->registers.MBR, \
	      cpu->registers.PC, cpu->registers.InReg, cpu->registers.OutReg)

//#define DEBUG() printf("%s: %s, %i\n", __FILE__, __func__, __LINE__)
#define DEBUG()

static void
marie_store_unsigned_integer (unsigned char *addr, int len, unsigned long val)
{
  unsigned char * p;
  unsigned char * startaddr = (unsigned char *)addr;
  unsigned char * endaddr = startaddr + len;

  for (p = endaddr; p > startaddr;)
    {
      * -- p = val & 0xff;
      val >>= 8;
    }
}
static 
int
marie_reg_fetch(
                SIM_CPU *scpu,
                int rn,
                unsigned char *memory,
                int length
                )
{
	DEBUG();
	//printf("rn: %i\tval:%i\n", rn, scpu->registers.regs[rn]);
	if (rn < NUM_MARIE_REGS && rn >= 0) {
		unsigned_word val = scpu->registers.regs[rn];

		/* misalignment-safe */
		marie_store_unsigned_integer (memory, 4, val);
		return length;
	}
	return 0;
}

static unsigned long
marie_extract_unsigned_integer (unsigned char *addr, int len)
{
  unsigned long retval;
  unsigned char * p;
  unsigned char * startaddr = (unsigned char *)addr;
  unsigned char * endaddr = startaddr + len;
 
  if (len > (int) sizeof (unsigned long))
    printf ("That operation is not available on integers of more than %zu bytes.",
	    sizeof (unsigned long));
 
  /* Start at the most significant end of the integer, and work towards
     the least significant.  */
  retval = 0;

  for (p = endaddr; p > startaddr;)
    retval = (retval << 8) | * -- p;
  
  return retval;
}

static 
int
marie_reg_store(
                SIM_CPU *scpu,
                int rn,
                unsigned char *memory,
                int length
                )
{
	DEBUG();
	if (rn < NUM_MARIE_REGS && rn >= 0)
	{
		unsigned_word ival;

		/* misalignment safe */
		ival = marie_extract_unsigned_integer (memory, length);
		scpu->registers.regs[rn] = ival;

		return length;
	}
	return 0;
}
static 
sim_cia
marie_pc_get(
             sim_cpu *cpu
             )
{
	DEBUG();
	//printf("current pc: %i\n", cpu->registers.PC);
	return cpu->registers.PC;
}

static 
void
marie_pc_set(
             sim_cpu *cpu,
						 sim_cia pc
             )
{
	DEBUG();
	//printf("current: %i\tnew:%i\n", cpu->registers.PC, pc);
  cpu->registers.PC = pc;
}


SIM_RC 
sim_create_inferior(
                    SIM_DESC sd,
                    struct bfd *prog_bfd,
                    char *const *argv, /* wha...? */
                    char *const *env
                    )
{
	sim_cpu *cpu = sd->cpu[0];
  if (prog_bfd != NULL)
    cpu->registers.PC = bfd_get_start_address (prog_bfd);
	return SIM_RC_OK;
}

SIM_DESC
sim_open(
         SIM_OPEN_KIND kind,
         host_callback *cb,
         struct bfd *abfd,
         char *const *argv
         )
{
	SIM_DESC sd = sim_state_alloc (kind, cb);

  /* The cpu data is kept in a separately allocated chunk of memory.  */
  if (sim_cpu_alloc_all (sd, 1, /*cgen_cpu_max_extra_bytes ()*/0) != SIM_RC_OK)
		goto cpu_fail;

	if (sim_pre_argv_init (sd, argv[0]) != SIM_RC_OK)
		goto argv_fail;
	if (sim_parse_args (sd, argv) != SIM_RC_OK)
		goto argv_fail;

	sim_do_command(sd," memory region 0x0000,0x2000") ; // these are real 8 bit bytes, while the marie architecture functionally has 16 bit bytes

	if (sim_analyze_program (sd,
	       (STATE_PROG_ARGV (sd) != NULL
	        ? *STATE_PROG_ARGV (sd)
	        : NULL), abfd) != SIM_RC_OK)
		goto argv_fail;
	if (sim_config (sd) != SIM_RC_OK)
		goto argv_fail;
	if (sim_post_argv_init (sd) != SIM_RC_OK)
		goto argv_fail;

	for (int i=0; i < MAX_NR_PROCESSORS; ++i) {
		SIM_CPU *cpu = STATE_CPU(sd, i);
		CPU_PC_FETCH(cpu)  = marie_pc_get;
		CPU_PC_STORE(cpu)  = marie_pc_set;
		CPU_REG_FETCH(cpu) = marie_reg_fetch;
		CPU_REG_STORE(cpu) = marie_reg_store;
	}

	return sd;

argv_fail:
	if (STATE_MODULES(sd) != NULL)
		sim_module_uninstall(sd);

cpu_fail:
	sim_cpu_free_all(sd);

	sim_state_free(sd);
	return 0;
}

/*
 * Start the engine!
 */
extern
void
sim_engine_run(
               SIM_DESC sd,
               int next_cpu_nr,
               int nr_cpus,
               int siggnal /* most simulators ignore siggnal */
               )
{
	setvbuf (stdout, NULL, _IONBF, 0);
	SIM_CPU *cpu = STATE_CPU(sd, 0); // TODO: more than one cpu?

	//printf("%p, %p\n", &cpu->registers.regs[3], &cpu->registers.MAR);

	int i = 0;
	do {
		// zero in and out registers as they are more akin to streams
		cpu->registers.InReg = 0;
		cpu->registers.OutReg = 0;

		cpu->registers.IR.byte = sim_core_read_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, CPU_PC_GET(cpu) * 2);
		DEBUG();
		switch (cpu->registers.IR.inst.opc) {
			case M_JNS: {
				MARIE_TRACE_INSN("JnS");
				cpu->registers.MBR = cpu->registers.PC + 1;
				cpu->registers.MAR = cpu->registers.IR.inst.arg;
				sim_core_write_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2, cpu->registers.MBR);
				cpu->registers.MBR = cpu->registers.IR.inst.arg;
				cpu->registers.AC = 1;
				cpu->registers.AC += cpu->registers.MBR;
				cpu->registers.PC = cpu->registers.AC;
				continue;
				break;
			}

			case M_LOAD: {
				MARIE_TRACE_INSN("Load");
				cpu->registers.MAR = cpu->registers.IR.inst.arg;
				cpu->registers.MBR = sim_core_read_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2);
				cpu->registers.AC = cpu->registers.MBR;
				MARIE_TRACE_INSN("Load");
				break;
			}
			case M_STORE: {
				MARIE_TRACE_INSN("Store");
				cpu->registers.MAR = cpu->registers.IR.inst.arg;
				cpu->registers.MBR = cpu->registers.AC;
				sim_core_write_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2, cpu->registers.MBR);
				break;
			}
			case M_ADD: {
				MARIE_TRACE_INSN("Add");
				cpu->registers.MAR = cpu->registers.IR.inst.arg;
				cpu->registers.MBR = sim_core_read_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2);
				cpu->registers.AC += cpu->registers.MBR;

				break;
			}
			case M_SUBT: {
				MARIE_TRACE_INSN("Subt");
				cpu->registers.MAR = cpu->registers.IR.inst.arg;
				cpu->registers.MBR = sim_core_read_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2);
				cpu->registers.AC -= cpu->registers.MBR;
				break;
			}
			case M_INPUT: {
				MARIE_TRACE_INSN("Input");
				cpu->registers.AC = getchar();
				break;
			}
			case M_OUTPUT: {
				MARIE_TRACE_INSN("Output");
				cpu->registers.OutReg = cpu->registers.AC;
				putchar(cpu->registers.OutReg);
				break;
		 }
			case M_HALT: {
				MARIE_TRACE_INSN("Halt");
				sim_engine_halt (sd, cpu, NULL, cpu->registers.PC, sim_exited, cpu->registers.AC);
				break;
			}
			case M_SKIPCOND: {
				MARIE_TRACE_INSN("Skipcond");
				bool skip = false;
				char comparator = (cpu->registers.IR.inst.arg & 0xc00) >> 10;
				switch (comparator) {
					case 0b00:
						if (((signed_word) cpu->registers.AC) < 0)
							skip = true;
						break;
					case 0b01:
						if (cpu->registers.AC == 0)
							skip = true;
						break;
					case 0b10:
						if (((signed_word) cpu->registers.AC) > 0)
							skip = true;
						break;
					default:
						sim_engine_halt (sd, cpu, NULL, cpu->registers.PC, sim_stopped, SIM_SIGILL);
						break;
				}
				if (skip)
					cpu->registers.PC++;
				break;
			}

			case  M_JUMP: {
				MARIE_TRACE_INSN("Jump");
				cpu->registers.PC = cpu->registers.IR.inst.arg;
				continue;
				break;
			}
			case M_CLEAR: {
				MARIE_TRACE_INSN("Clear");
				cpu->registers.AC = 0;
				break;
			}
			case M_ADDI: {
				MARIE_TRACE_INSN("AddI");
				cpu->registers.MAR = cpu->registers.IR.inst.arg;
				cpu->registers.MBR = sim_core_read_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2);
				cpu->registers.MAR = cpu->registers.MBR;
				cpu->registers.MBR = sim_core_read_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2);
				cpu->registers.AC += cpu->registers.MBR;
				break;
			}
			case M_JUMPI: {
				MARIE_TRACE_INSN("JumpI");
				cpu->registers.MAR = cpu->registers.IR.inst.arg;
				cpu->registers.MBR = sim_core_read_aligned_2(cpu, CPU_PC_GET(cpu) * 2, read_map, cpu->registers.MAR * 2);
				cpu->registers.PC = cpu->registers.MBR;
				continue;
				break;
			}
		default:
			MARIE_TRACE_INSN("all");
			sim_engine_halt (sd, cpu, NULL, cpu->registers.PC, sim_stopped, SIM_SIGILL);
			//continue;
			break;
		}
		cpu->registers.PC++;

		if (sim_events_tick (sd))
			sim_events_process (sd);
	} while (1);
}
